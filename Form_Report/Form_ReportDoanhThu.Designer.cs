﻿namespace QLKS_EF.Form_Report
{
	partial class Form_ReportDoanhThu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
			this.quanLyKhachSanDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.quanLyKhachSanDataSet = new QLKS_EF.QuanLyKhachSanDataSet();
			this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
			this.HoaDonBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.hoaDonBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.hoaDonTableAdapter = new QLKS_EF.QuanLyKhachSanDataSetTableAdapters.HoaDonTableAdapter();
			this.quanLyKhachSanDataSet1 = new QLKS_EF.QuanLyKhachSanDataSet();
			this.hoaDonBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
			this.hoaDonBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
			((System.ComponentModel.ISupportInitialize)(this.quanLyKhachSanDataSetBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.quanLyKhachSanDataSet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.HoaDonBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.hoaDonBindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.quanLyKhachSanDataSet1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.hoaDonBindingSource2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.hoaDonBindingSource3)).BeginInit();
			this.SuspendLayout();
			// 
			// quanLyKhachSanDataSetBindingSource
			// 
			this.quanLyKhachSanDataSetBindingSource.DataSource = this.quanLyKhachSanDataSet;
			this.quanLyKhachSanDataSetBindingSource.Position = 0;
			// 
			// quanLyKhachSanDataSet
			// 
			this.quanLyKhachSanDataSet.DataSetName = "QuanLyKhachSanDataSet";
			this.quanLyKhachSanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// reportViewer1
			// 
			this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			reportDataSource1.Name = "DataSet_HoaDon";
			reportDataSource1.Value = this.HoaDonBindingSource;
			this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
			this.reportViewer1.LocalReport.ReportEmbeddedResource = "QLKS_EF.Form_Report.Report_DoanhThuTheoNV.rdlc";
			this.reportViewer1.Location = new System.Drawing.Point(0, 0);
			this.reportViewer1.Name = "reportViewer1";
			this.reportViewer1.ServerReport.BearerToken = null;
			this.reportViewer1.Size = new System.Drawing.Size(800, 450);
			this.reportViewer1.TabIndex = 0;
			// 
			// HoaDonBindingSource
			// 
			this.HoaDonBindingSource.DataMember = "HoaDon";
			this.HoaDonBindingSource.DataSource = this.quanLyKhachSanDataSet;
			// 
			// hoaDonBindingSource1
			// 
			this.hoaDonBindingSource1.DataMember = "HoaDon";
			this.hoaDonBindingSource1.DataSource = this.quanLyKhachSanDataSetBindingSource;
			// 
			// hoaDonTableAdapter
			// 
			this.hoaDonTableAdapter.ClearBeforeFill = true;
			// 
			// quanLyKhachSanDataSet1
			// 
			this.quanLyKhachSanDataSet1.DataSetName = "QuanLyKhachSanDataSet";
			this.quanLyKhachSanDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// hoaDonBindingSource2
			// 
			this.hoaDonBindingSource2.DataMember = "HoaDon";
			this.hoaDonBindingSource2.DataSource = this.quanLyKhachSanDataSet1;
			// 
			// hoaDonBindingSource3
			// 
			this.hoaDonBindingSource3.DataMember = "HoaDon";
			this.hoaDonBindingSource3.DataSource = this.quanLyKhachSanDataSet1;
			// 
			// Form_ReportDoanhThu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.reportViewer1);
			this.Name = "Form_ReportDoanhThu";
			this.Text = "Form_ReportDoanhThu";
			this.Load += new System.EventHandler(this.Form_ReportDoanhThu_Load);
			((System.ComponentModel.ISupportInitialize)(this.quanLyKhachSanDataSetBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.quanLyKhachSanDataSet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.HoaDonBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.hoaDonBindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.quanLyKhachSanDataSet1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.hoaDonBindingSource2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.hoaDonBindingSource3)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
		private System.Windows.Forms.BindingSource quanLyKhachSanDataSetBindingSource;
		private QuanLyKhachSanDataSet quanLyKhachSanDataSet;
		private System.Windows.Forms.BindingSource HoaDonBindingSource;
		private System.Windows.Forms.BindingSource hoaDonBindingSource1;
		private QuanLyKhachSanDataSetTableAdapters.HoaDonTableAdapter hoaDonTableAdapter;
		private System.Windows.Forms.BindingSource hoaDonBindingSource2;
		private QuanLyKhachSanDataSet quanLyKhachSanDataSet1;
		private System.Windows.Forms.BindingSource hoaDonBindingSource3;
	}
}