﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Xml;

namespace QLKS_EF.BS_Layer
{
	class BL_NhanVien
	{
		public object SqlMethods { get; private set; }

		public bool DangNhap(string Email, string Password, string ChucVu)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			if (ChucVu == "Admin")
			{
				var query = (from nv in QLKSEntities.NhanViens where nv.Email == Email & nv.ChucVu == "Admin" select nv).SingleOrDefault();
				if (query != null)
				{
					if (Password == query.MatKhau)
					{
						return true;
					}
					return false;
				}
			}
			else
			{
				var query = (from nv in QLKSEntities.NhanViens where nv.Email == Email & nv.ChucVu == "NhanVien" select nv).SingleOrDefault();
				if (query != null)
				{
					if (Password == query.MatKhau)
					{
						return true;
					}
					return false;
				}
			}
			return false;
		}

		public bool XacThucNhanVien(string Email, string SDT)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from nv in QLKSEntities.NhanViens where nv.Email == Email & nv.SDT == SDT select nv).SingleOrDefault();
			if (query != null)
			{
				return true;
			}
			return false;
		}

		public bool DoiMatKhau(string Email, string MatKhauMoi)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from nv in QLKSEntities.NhanViens where nv.Email == Email select nv).SingleOrDefault();
			if (query != null)
			{
				query.MatKhau = MatKhauMoi;
				QLKSEntities.SaveChanges();
				return true;
			}
			return false;
		}

		public NhanVien LayNhanVien_Email(string Email)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from nv in QLKSEntities.NhanViens where nv.Email == Email select nv).SingleOrDefault();
			if (query != null)
			{
				return query;
			}
			return null;
		}

		public NhanVien LayNhienVien_MaNV(int manv)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from nv in QLKSEntities.NhanViens where nv.MaNV == manv select nv).SingleOrDefault();
			if (query != null)
			{
				return query;
			}
			return null;
		}

		public void ThemNhanVien(string HoTen, string DiaChi, string Email, string SDT, string MatKhau, string ChucVu)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			NhanVien nv = new NhanVien();
			nv.HoTen = HoTen;
			nv.DiaChi = DiaChi;
			nv.Email = Email;
			nv.SDT = SDT;
			nv.MatKhau = MatKhau;
			nv.ChucVu = ChucVu;
			QLKSEntities.NhanViens.Add(nv);
			QLKSEntities.SaveChanges();
		}

		public void SuaNhanVien(int manv, string HoTen, string DiaChi, string Email, string SDT, string MatKhau, string ChucVu)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from n in QLKSEntities.NhanViens where n.MaNV == manv select n).FirstOrDefault();
			query.HoTen = HoTen;
			query.DiaChi = DiaChi;
			query.Email = Email;
			query.SDT = SDT;
			query.MatKhau = MatKhau;
			query.ChucVu = ChucVu;
			QLKSEntities.SaveChanges();
		}

		public void XoaNhanVien(int manv)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from n in QLKSEntities.NhanViens where n.MaNV == manv select n).FirstOrDefault();
			QLKSEntities.NhanViens.Remove(query);
			QLKSEntities.SaveChanges();
		}

		public DataTable LayDSNhanVien()
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var nhanviens = from n in QLKSEntities.NhanViens select n;
			DataTable dt = new DataTable();
			dt.Columns.Add("Mã nhân viên");
			dt.Columns.Add("Họ tên");
			dt.Columns.Add("Địa chỉ");
			dt.Columns.Add("SDT");
			dt.Columns.Add("Email");
			dt.Columns.Add("Chức vụ");
			foreach (var nv in nhanviens)
			{
				dt.Rows.Add(nv.MaNV, nv.HoTen, nv.DiaChi, nv.SDT, nv.Email, nv.ChucVu);
			}
			return dt;
		}
		public DataTable TimKiemNhanVien(string text)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			DataTable dt = new DataTable();
			var nhanviens = from n in QLKSEntities.NhanViens
							where n.HoTen.Contains(text) || n.DiaChi.Contains(text) || n.Email.Contains(text) || n.SDT.Contains(text) || n.ChucVu.Contains(text)
							select n;
			dt.Columns.Add("Mã nhân viên");
			dt.Columns.Add("Họ tên");
			dt.Columns.Add("Địa chỉ");
			dt.Columns.Add("SDT");
			dt.Columns.Add("Email");
			dt.Columns.Add("Chức vụ");
			foreach (var nv in nhanviens)
			{
				dt.Rows.Add(nv.MaNV, nv.HoTen, nv.DiaChi, nv.SDT, nv.Email, nv.ChucVu);
			}
			return dt;
		}
	}
}
