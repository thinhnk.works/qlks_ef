﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_EF.BS_Layer
{
	class BL_KhachHang
	{
		public DataTable DanhSachKhachHang()
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var khs = from p in QLKSEntities.KhachHangs select p;
			if (khs != null)
			{
				DataTable dt = new DataTable();
				dt.Columns.Add("Mã KH");
				dt.Columns.Add("Họ tên");
				dt.Columns.Add("Địa chỉ");
				dt.Columns.Add("SĐT");
				dt.Columns.Add("CCCD");
				dt.Columns.Add("Email");
				dt.Columns.Add("Số lần nghỉ");
				dt.Columns.Add("Khách VIP");
				foreach (var p in khs)
				{
					dt.Rows.Add(p.MaKH, p.HoTen, p.DiaChi, p.SDT, p.CCCD, p.Email, p.SoLanNghi, p.KhachVIP);
				}
				return dt;
			}
			return null;
		}
		public DataTable TimKiemKhachHang(string text)
		{
            QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
            var khs = from p in QLKSEntities.KhachHangs
					  where p.HoTen.Contains(text) || p.DiaChi.Contains(text) || p.SDT.Contains(text) || p.CCCD.Contains(text) || p.Email.Contains(text) || p.KhachVIP.ToString().Contains(text)
					  select p;
            if (khs != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Mã KH");
                dt.Columns.Add("Họ tên");
                dt.Columns.Add("Địa chỉ");
                dt.Columns.Add("SĐT");
                dt.Columns.Add("CCCD");
                dt.Columns.Add("Email");
                dt.Columns.Add("Số lần nghỉ");
                dt.Columns.Add("Khách VIP");
                foreach (var p in khs)
                {
                    dt.Rows.Add(p.MaKH, p.HoTen, p.DiaChi, p.SDT, p.CCCD, p.Email, p.SoLanNghi, p.KhachVIP);
                }
                return dt;
            }
            return null;
        }
		public KhachHang TimKhachHang(string CCCD)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from khachHang in QLKSEntities.KhachHangs where khachHang.CCCD == CCCD select khachHang).FirstOrDefault();
			if (query != null)
			{
				return query;
			}
			return null;
		}
		public KhachHang ThemKhachHang(string HoTen, string DiaChi, string Email, string SDT, string CCCD)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from khachHang in QLKSEntities.KhachHangs where khachHang.CCCD == CCCD select khachHang).FirstOrDefault();
			// Đã có KH này trong db  --> Cộng 1 vào số lần nghỉ
			if (query != null)
			{
				query.SoLanNghi = query.SoLanNghi + 1;
				QLKSEntities.SaveChanges();
				return query;
			}
			// KH không có sẵn trong db  -->  Tạo mới
			else
			{
				KhachHang kh = new KhachHang();
				kh.HoTen = HoTen;
				kh.DiaChi = DiaChi;
				kh.SDT = SDT;
				kh.Email = Email;
				kh.CCCD = CCCD;
				kh.SoLanNghi = 1;
				kh.KhachVIP = false;
				QLKSEntities.KhachHangs.Add(kh);
				QLKSEntities.SaveChanges();
				return kh;
			}
		}
		public void SuaKhachHang(int maKH, string HoTen, string DiaChi, string Email, string SDT, string CCCD)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var kh = (from khachHang in QLKSEntities.KhachHangs where khachHang.MaKH == maKH select khachHang).FirstOrDefault();
			if (kh != null)
			{
				kh.HoTen = HoTen;
				kh.DiaChi = DiaChi;
				kh.Email = Email;
				kh.SDT = SDT;
				kh.CCCD = CCCD;
				QLKSEntities.SaveChanges();
			}
		}
		public void XoaKhachHang(int MaKH)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			// Tìm KH
			var query = (from p in QLKSEntities.KhachHangs where p.MaKH == MaKH select p).FirstOrDefault();
			// Kiểm tra xem KH đó có đang thuê phòng hay không
			// Nếu có thì ko được xóa
			var dsDatPhong = (from _dp in QLKSEntities.DatPhongs select _dp);
			foreach (var dp in dsDatPhong)
			{
				if (dp.MaKH == query.MaKH && dp.TraPhong == false)
				{
					MessageBox.Show("Không được xóa khách hàng đang thuê", "Thông báo", MessageBoxButtons.OK);
					return;
				}
				if (dp.MaKH == query.MaKH && dp.TraPhong == true)
				{
					dp.MaKH = null;
				}
			}
			QLKSEntities.KhachHangs.Remove(query);
			QLKSEntities.SaveChanges();
		}
	}
}
