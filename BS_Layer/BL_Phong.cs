﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_EF.BS_Layer
{

	class MyPhong
	{
		public MyPhong() { }
		private string maPhong;
		public string MaPhong
		{
			get { return maPhong; }
			set { maPhong = value; }
		}

		private int tinhTrang;
		public int TinhTrang
		{
			get { return tinhTrang; }
			set { tinhTrang = value; }
		}

		private float gia;
		public float Gia
		{
			get { return gia; }
			set { gia = value; }
		}

		private int sucChua;
		public int SucChua
		{
			get { return sucChua; }
			set { sucChua = value; }
		}

		private string moTa;
		public string MoTa
		{
			get { return moTa; }
			set { moTa = value; }
		}

		private string tenLoaiPhong;
		public string TenLoaiPhong
		{
			get { return tenLoaiPhong; }
			set { tenLoaiPhong = value; }
		}
	}

	class BL_Phong
	{
		public List<MyPhong> LayDSPhong()
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			List<MyPhong> DanhSachPhong = new List<MyPhong>();
			var phongs = from p in QLKSEntities.Phongs
						 join lp in QLKSEntities.LoaiPhongs
						 on p.LoaiPhong equals lp
						 select new
						 {
							 p.MaPhong,
							 p.LoaiPhong,
							 p.TinhTrang,
							 lp.TenLoaiPhong,
							 lp.MoTa,
							 lp.Gia,
							 lp.SucChua
						 };
			foreach (var p in phongs)
			{
				MyPhong mp = new MyPhong();
				mp.MoTa = p.MoTa;
				mp.Gia = (float)(p.Gia);
				mp.TenLoaiPhong = p.TenLoaiPhong;
				mp.SucChua = (int)p.SucChua;
				mp.MaPhong = p.MaPhong;
				mp.TinhTrang = (int)p.TinhTrang;
				DanhSachPhong.Add(mp);
			}
			return DanhSachPhong;
		}
		public List<MyPhong> LayDSPhongFilter(int tinhTrangPhong)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			List<MyPhong> DanhSachPhong = new List<MyPhong>();
			var phongs = from p in QLKSEntities.Phongs
						 join lp in QLKSEntities.LoaiPhongs
						 on p.LoaiPhong equals lp
						 // tinhTrang = 1  ==> Tìm phòng trống
						 // tinhTrang = 2  ==> Tìm phòng đang cho thuê
						 // tinhTrang = 3  ==> Tìm phòng đang dọn
						 where p.TinhTrang == tinhTrangPhong
						 select new
						 {
							 p.MaPhong,
							 p.LoaiPhong,
							 p.TinhTrang,
							 lp.TenLoaiPhong,
							 lp.MoTa,
							 lp.Gia,
							 lp.SucChua
						 };
			foreach (var p in phongs)
			{
				MyPhong mp = new MyPhong();
				mp.MoTa = p.MoTa;
				mp.Gia = (float)(p.Gia);
				mp.TenLoaiPhong = p.TenLoaiPhong;
				mp.SucChua = (int)p.SucChua;
				mp.MaPhong = p.MaPhong;
				mp.TinhTrang = (int)p.TinhTrang;
				DanhSachPhong.Add(mp);
			}
			return DanhSachPhong;
		}

		public static void TraPhong(string maphong)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			List<MyPhong> DanhSachPhong = new List<MyPhong>();
			var phongs = from p in QLKSEntities.Phongs
						 join lp in QLKSEntities.LoaiPhongs
						 on p.LoaiPhong equals lp
						 where p.MaPhong == maphong
						 select p;
			// Tình trạng chuyển về 3 (đang dọn)
			phongs.SingleOrDefault().TinhTrang = 3;
			DatPhong datphong = (from _dp in QLKSEntities.DatPhongs orderby _dp.MaDatPhong descending select _dp).FirstOrDefault();
			datphong.TraPhong = true;
			QLKSEntities.SaveChanges();
		}

		// Tình trạng chuyển về 1 (dọn xong - sẵn sàng cho thuê)
		public static void DonPhong(string maphong)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			List<MyPhong> DanhSachPhong = new List<MyPhong>();
			var phongs = from p in QLKSEntities.Phongs
						 join lp in QLKSEntities.LoaiPhongs
						 on p.LoaiPhong equals lp
						 where p.MaPhong == maphong
						 select p;
			phongs.SingleOrDefault().TinhTrang = 1;
			QLKSEntities.SaveChanges();
		}

		// Khách hàng yêu cầu thêm dịch vụ
		public static void ThemDichVu(int maDatPhong, int madv, int soLuong)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();

			// Chi tiết Đặt phòng dùng để thêm 1 dịch vụ vào record Đặt phòng
			ChiTietDatPhong ctdp = new ChiTietDatPhong();

			// Tham chiếu tới Đặt phòng
			DatPhong dp = (from _dp in QLKSEntities.DatPhongs where _dp.MaDatPhong == maDatPhong select _dp).FirstOrDefault();
			ctdp.DatPhong = dp;

			// Thêm dịch vụ vào Chi tiết đặt phòng
			var dv = (from _dv in QLKSEntities.DichVus where _dv.MaDV == madv select _dv).FirstOrDefault();
			ctdp.DichVu = dv;
			ctdp.SoLuong = soLuong;
			ctdp.ThanhTien = (float)(dv.Gia * soLuong);

			// cộng tiền dịch vụ vào Tổng tiền của lần đặt phòng đó
			dp.TongTien += ctdp.ThanhTien;

			// Thêm chi tiết đặt phòng
			QLKSEntities.ChiTietDatPhongs.Add(ctdp);
			QLKSEntities.SaveChanges();
		}

		public void DatPhong(int MaKH, string maPhong, DateTime ngayDen, DateTime ngayDi, int soNguoiO, float tienCoc, int NVTiepNhan, int songayo)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			Phong p = (from _p in QLKSEntities.Phongs where _p.MaPhong == maPhong select _p).FirstOrDefault();

			DatPhong dp = new DatPhong();
			dp.MaKH = MaKH;
			dp.MaPhong = maPhong;
			dp.NgayDen = ngayDen;
			dp.NgayDi = ngayDi;
			dp.SoNguoiO = soNguoiO;
			dp.TienCoc = tienCoc;
			dp.NVTiepNhan = NVTiepNhan;
			dp.SoNgayO = songayo;
			dp.TraPhong = false;

			// Tổng tiền = Số ngày x Giá
			dp.TongTien = songayo * p.LoaiPhong.Gia - tienCoc;

			QLKSEntities.DatPhongs.Add(dp);
			QLKSEntities.SaveChanges();

			// Chuyển trạng thái thuê từ 1 sang 2 (trống --> đã có người thuê)
			var phongs = from _p in QLKSEntities.Phongs
						 join lp in QLKSEntities.LoaiPhongs
						 on _p.LoaiPhong equals lp
						 where _p.MaPhong == maPhong
						 select _p;
			phongs.FirstOrDefault().TinhTrang = 2;
			QLKSEntities.SaveChanges();
		}

		public static HoaDon XuatHoaDon(int maDatPhong, int makh, int manv)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from x in QLKSEntities.DatPhongs where x.MaDatPhong == maDatPhong select x).FirstOrDefault();

			// Gán ngày đi
			query.NgayDi = DateTime.UtcNow.Date;
			// Tính ngày ở thực tế
			int songay = ((TimeSpan)(query.NgayDi - query.NgayDen)).Days + 1;
			// Nếu rời đi sớm hơn dự tính --> vẫn tính tiền theo số ngày ban đầu
			if (songay < query.SoNgayO)
			{
				songay = (int)query.SoNgayO;
			}
			// Xóa tiền nghỉ gốc (không xóa tiền dịch vụ)
			query.TongTien -= query.Phong.LoaiPhong.Gia * query.SoNgayO;
			// Cộng tiền nghỉ theo số ngày nghỉ thực tế
			query.TongTien += query.Phong.LoaiPhong.Gia * songay;

			// Lập hóa đơn
			HoaDon hd = new HoaDon();
			hd.MaDatPhong = maDatPhong;
			hd.DatPhong = query;
			hd.NgayLap = DateTime.UtcNow.Date;
			hd.TongTien = query.TongTien;
			hd.NVThanhToan = manv;

			QLKSEntities.HoaDons.Add(hd);
			QLKSEntities.SaveChanges();

			HoaDon h = (from x in QLKSEntities.HoaDons orderby x.MaHoaDon descending select x).FirstOrDefault();
			return h;
		}

		public static DatPhong LayDatPhong(string maphong)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			DatPhong dp = (from _dp in QLKSEntities.DatPhongs where _dp.MaPhong == maphong & _dp.Phong.TinhTrang == 2 orderby _dp.MaDatPhong descending select _dp).FirstOrDefault();
			return dp;
		}
	}
}
