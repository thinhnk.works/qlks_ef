﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_EF.BS_Layer
{
	public class ChiTietDichVu
	{
		public int MaDV { get; set; }
		public string TenDV { get; set; }
		public float Gia { get; set; }
		public int SoLuong { get; set; }
	}
	class BL_DichVu
	{
		public DataTable DSDichVu()
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var dvs = from d in QLKSEntities.DichVus select d;

			DataTable dt = new DataTable();
			dt.Columns.Add("Mã dịch vụ");
			dt.Columns.Add("Tên dịch vụ");
			dt.Columns.Add("Giá");
			foreach (var dv in dvs)
			{
				dt.Rows.Add(dv.MaDV, dv.TenDV, dv.Gia);
			}
			return dt;
		}
		public DataTable TimKiemDichVu(string text)
		{

            QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var dvs = from d in QLKSEntities.DichVus
					  where d.TenDV.Contains(text) || d.Gia.ToString().Contains(text)
					  select d;

            DataTable dt = new DataTable();
            dt.Columns.Add("Mã dịch vụ");
            dt.Columns.Add("Tên dịch vụ");
            dt.Columns.Add("Giá");
            foreach (var dv in dvs)
            {
                dt.Rows.Add(dv.MaDV, dv.TenDV, dv.Gia);
            }
            return dt;
        }
		public void TaoDichVu(string tendv, float gia)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			DichVu dv = new DichVu();
			dv.TenDV = tendv;
			dv.Gia = gia;
			QLKSEntities.DichVus.Add(dv);
			QLKSEntities.SaveChanges();
		}
		public void SuaDichVu(int madv, string tendv, float gia)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from d in QLKSEntities.DichVus where d.MaDV == madv select d).FirstOrDefault();
			query.TenDV = tendv;
			query.Gia = gia;
			QLKSEntities.SaveChanges();
		}
		public void XoaDichVu(int madv)
		{
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = (from d in QLKSEntities.DichVus where d.MaDV == madv select d).FirstOrDefault();
			QLKSEntities.DichVus.Remove(query);
			QLKSEntities.SaveChanges();
		}
		public static List<ChiTietDichVu> ListDichVu(int madatphong)
		{
			List<ChiTietDichVu> ldv = new List<ChiTietDichVu>();
			QuanLyKhachSanEntities QLKSEntities = new QuanLyKhachSanEntities();
			var query = from _dp in QLKSEntities.DatPhongs
						join _ctdp in QLKSEntities.ChiTietDatPhongs
						on _dp.MaDatPhong equals _ctdp.MaDatPhong
						where _dp.MaDatPhong == madatphong
						select _ctdp;
			foreach (var q in query)
			{
				ChiTietDichVu ctdv = new ChiTietDichVu();
				ctdv.MaDV = (int)q.MaDV;
				ctdv.TenDV = q.DichVu.TenDV;
				ctdv.SoLuong = (int)q.SoLuong;
				ctdv.Gia = (float)q.ThanhTien;
				ldv.Add(ctdv);
			}
			return ldv;
		}
	}
}
