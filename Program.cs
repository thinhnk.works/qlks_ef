using QLKS_EF.Form_BatDau;
using QLKS_EF.Form_NhanVien;
using QLKS_EF.Form_QuanLy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKS_EF
{
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Form form = new Form_DangNhap();
			Application.Run(form);
		}
	}
}
